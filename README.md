# Synopsis

Free SSL certificate from Let's Encrypt using certbot.

# Motivation

Free SSL certificate for you https website.

# Installation

```bash
./install
```
If you do not want any installation but certificate itself. You can use [Get HTTPS for free!](https://gethttpsforfree.com/) to generate your own certificate for free!
It is very simple and intuitive.

# Tests

Start running your web server and open your page.

# Contributor

Jason Chen
